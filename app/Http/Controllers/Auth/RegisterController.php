<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Events\RegisterMailNotifEvent;
use App\Mail\UserRegisterMail;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'password' => 'required'
        ]);

        // $data_request = $request->all();
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);
        $data['user'] = $user;

        event(new RegisterMailNotifEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User baru sudah terdaftar, Silahkan cek email anda untuk verifikasi dengan kode otp',
            'data' => $data
        ]);
    }
}
