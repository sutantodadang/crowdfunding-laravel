<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Carbon\Carbon;
use App\User;
use Illuminate\Http\Request;

class SocialLiteController extends Controller
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider)
    {
        try {
            //code...
            $social_user = Socialite::driver($provider)->stateless()->user();

            if (!$social_user) {
                # code...
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'login gagal'

                ], 401);
            }


            $user = User::whereEmail($social_user->email)->first();

            if (!$user) {
                if ($provider == 'google') {
                    # code...
                    $photo_profile = $social_user->avatar;
                }
                $user = User::create([
                    'email' => $social_user->email,
                    'name' => $social_user->name,
                    'email_verified_at' => Carbon::now(),
                    'photo' => $photo_profile
                ]);
            }


            $data['user'] = $user;
            $data['token'] = auth()->login($user);

            return response()->json([
                'response_code' => '00',
                'response_message' => 'login berhasil',
                'data' => $data
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([
                'response_code' => '01',
                'response_message' => 'login gagal',
                'data' => $th

            ], 401);
        }
    }
}
