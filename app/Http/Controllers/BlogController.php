<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count)
    {
        $blogs = Blog::select('*')->inRandomOrder()->limit($count)->get();

        $data['blog'] = $blogs;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditampilkan',
            'data' => $data
        ], 200);
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg'
        ]);

        $blog = Blog::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        $data['campaigns'] = $blog;

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $image_extension = $image->getClientOriginalExtension();

            $image_name = $blog->id . "." . $image_extension;

            $image_folder = '/storage/blog/';

            $image_location = $image_folder . $image_name;

            try {
                //code...
                $image->move(public_path($image_folder), $image_name);

                $blog->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                //throw $th;
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo gagal upload',
                    'data' => $data
                ], 200);
            }
        }





        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditampilkan',
            'data' => $data
        ], 200);
    }
}
