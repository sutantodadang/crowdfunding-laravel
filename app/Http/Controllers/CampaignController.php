<?php

namespace App\Http\Controllers;

use App\Campaign;
use Illuminate\Http\Request;
use PDO;
use Illuminate\Support\Str;


class CampaignController extends Controller
{

    public function index()
    {
        $campaigns = Campaign::paginate(6);

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function random($count)
    {
        $campaigns = Campaign::select('*')->inRandomOrder()->limit($count)->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg'
        ]);

        $campaign = Campaign::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        $data['campaigns'] = $campaign;

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $image_extension = $image->getClientOriginalExtension();

            $image_name = $campaign->id . "." . $image_extension;

            $image_folder = '/storage/campaign/';

            $image_location = $image_folder . $image_name;

            try {
                //code...
                $image->move(public_path($image_folder), $image_name);

                $campaign->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e) {
                //throw $th;
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo gagal upload',
                    'data' => $data
                ], 200);
            }
        }





        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function detail($id)
    {
        $campaign = Campaign::find($id);

        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaign berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function search($keyword)
    {
        $campaigns = Campaign::select('*')->where('title', 'LIKE', "%" . $keyword . "%")->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaign berhasil ditampilkan',
            'data' => $data
        ], 200);
    }
}
