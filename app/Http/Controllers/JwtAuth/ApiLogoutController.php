<?php

namespace App\Http\Controllers\JwtAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiLogoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        auth()->logout();
    }
}
