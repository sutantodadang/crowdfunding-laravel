<?php

namespace App\Http\Controllers\JwtAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\RegisterRequest;
use App\OtpCode;
use Carbon\Carbon;
use Auth;

class ApiRegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

        $user = auth()->user();
        Carbon::setLocale('id');
        $otpcode = mt_rand(000000, 999999);
        $valid = Carbon::now()->addMinutes(300)->toDateTimeString();

        auth()->user()->create([
            'otp' => $otpcode,
            'valid_until' => $valid,
            'user_id' => $user->id
        ]);

        // $otp->save();

        return response()->json([
            'message code' => '00',
            'message' => 'Berhasil Registrasi',
            'otp' => $otpcode,
            'valid until' => $valid

        ]);
    }
}
