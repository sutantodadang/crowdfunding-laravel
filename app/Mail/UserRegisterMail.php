<?php

namespace App\Mail;

use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class UserRegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    public $otp_code;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        // $this->otp_code = OtpCode::find()->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->view('mail.email_notif')
            ->with([
                'name' => $this->user->name
                // 'otp' => $this->otp_code->otp,
            ]);
    }
}
