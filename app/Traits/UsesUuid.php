<?php


namespace App\Traits;

use Illuminate\Support\Str;

trait UsesUuid
{


    // membuat event untuk menambahkan data id otomatis 
    public static function bootUsesUuid()
    {
        static::creating(function ($model) {
            if (!$model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    // membuat id tidak auto increment 
    public function getIncrementing()
    {
        return false;
    }

    // membuat type data id 
    public function getKeyType()
    {
        return 'string';
    }
}
