<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Traits\UsesUuid;
use App\OtpCode;
use App\Role;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;


    protected function get_role_id()
    {
        $role = Role::where('name', 'user')->first();
        return $role->id;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->role_id = $model->get_role_id();
        });

        static::created(function ($model) {
            $model->generate_otp();
        });
    }

    public function isAdmin()
    {
        if ($this->role_id === $this->get_role_id()) {
            return false;
        }
        return true;
    }

    public function isVerif()
    {
        if ($this->email_verified_at != null) {
            return true;
        }
        return false;
    }

    public function generate_otp()
    {
        do {
            $random = mt_rand(000000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        Carbon::setLocale('id');
        $now = Carbon::now();
        $otp = OtpCode::updateOrCreate(
            [
                'user_id' => $this->id
            ],
            ['otp' => $random, 'valid_until' => $now->addMinutes(5)]
        );
    }

    public function chats()
    {
        return $this->hasMany(Chat::class);
    }




    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function otp_code()
    {
        return $this->hasOne(OtpCode::class);
    }
}
