<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Chat;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
    return [
        'id' => (string) Str::uuid(),
        'subject' => $faker->name,
        'user_id' => User::all()->random()->id,

    ];
});
