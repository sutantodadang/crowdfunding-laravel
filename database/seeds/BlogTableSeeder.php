<?php

use App\Blog;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 10; $i++) {

            // insert data ke table pegawai menggunakan Faker
            Blog::create([
                'title' => $faker->sentence,
                'description' => $faker->paragraph,
                'image' => $faker->image('public/photos/blog/', 640, 480, null, false),

            ]);
        }
    }
}
