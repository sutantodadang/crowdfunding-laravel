<?php

use App\Campaign;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $campaign = Campaign::all();

        for ($i = 1; $i <= 10; $i++) {

            // insert data ke table menggunakan Faker
            Campaign::create([
                'title' => $faker->sentence,
                'description' => $faker->paragraph,
                'image' => $faker->image('public/photos/campaign/' + $campaign->id, 640, 480, null, false),

            ]);
        }
    }
}
