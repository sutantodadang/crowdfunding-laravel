<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail Notif</title>
    <style>
        .title {
            font-weight: 400;
        }

        .otp {
            justify-content: center;
            align-items: center;
        }
    </style>
</head>

<body>
    <div class="otp">
        <h3 class="title">Selamat Datang {{ $name }} di website kami</h3>
        <h2>Ini Kode OTP Anda: </h2>
        <h1> {{ $otp }} </h1>
    </div>

</body>

</html>