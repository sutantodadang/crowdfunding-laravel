<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::namespace('JwtAuth')->group(function () {
//     Route::post('register', 'ApiRegisterController');
//     Route::post('login', 'ApiLoginController');
//     Route::post('logout', 'ApiLogoutController');
// });

// Route::get('user', 'UserController');


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function () {
    Route::post('register', 'RegisterController');
    Route::post('regenerate', 'RegenerateController');
    Route::post('verification', 'VerificationController');
    Route::post('update-password', 'UpdatePasswordController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController')->middleware('auth:api');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');

    Route::get('/social/{provider}', 'SocialLiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialLiteController@handleProviderCallback');
});

Route::group([
    'middleware' => ['api', 'email_verif', 'auth:api']
], function () {
    Route::get('/profile/show', 'ProfileController@show');
    Route::post('/profile/update', 'ProfileController@update');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaign'
], function () {
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('/{id}', 'CampaignController@detail');
    Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blog'
], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'chat'
], function () {
    Route::view('chat', 'chat.index');
});
