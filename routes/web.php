<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::middleware(['auth', 'admin'])->group(function () {

//     Route::get('/route-1', 'HomeController@user');
//     Route::get('/route-2', 'HomeController@admin');
// });
// // tidak ditambahkan karena nanti tidak bisa logout 
// Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/', function () {
//     return view('app');
// });

Route::view('/{any?}', 'app')->where('any', '.*');

// Route::view('/chat', 'chat.index')->middleware('auth');



// Auth::routes();
